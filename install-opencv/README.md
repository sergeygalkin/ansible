Tested on ansible 1.5.4

For use with 2.1.1.0 you should run something like 
```
ansible-playbook -i ./hosts -b --become-user=root --ask-sudo-pass  ./install-opencv.yaml
```
and cnange in install-opencv.yaml
```
  sudo: yes
```
to
```
  become: true
  become_user: root
```
